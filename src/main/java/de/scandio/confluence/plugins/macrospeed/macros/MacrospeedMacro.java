package de.scandio.confluence.plugins.macrospeed.macros;

import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.WikiStyleRenderer;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * The Macrospeed macro measures the time Confluence needs to render the enclosing body and displays it on the page.
 *
 * @author Georg Schmidl <georg.schmidl@scandio.de>
 */
public class MacrospeedMacro extends BaseMacro {

    private WikiStyleRenderer wikiStyleRenderer;

    @Override
    public String execute(@SuppressWarnings("rawtypes") Map parameters, String body, RenderContext renderContext)
            throws MacroException {
        Long start = System.nanoTime();
        String renderedBody = wikiStyleRenderer.convertWikiToXHtml(renderContext, body);
        Long stop = System.nanoTime();

        double elapsedSeconds = 1.0 * (stop - start) / 1000 / 1000 / 1000;

        Map<String, Object> context = MacroUtils.defaultVelocityContext();
        context.put("body", renderedBody);
        context.put("elapsedSeconds", elapsedSeconds);

        return VelocityUtils.getRenderedTemplate("templates/macrospeed.vm", context);
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public boolean hasBody() {
        return true;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setWikiStyleRenderer(WikiStyleRenderer wikiStyleRenderer) {
        this.wikiStyleRenderer = wikiStyleRenderer;
    }

}